const express = require('express');
const routes = require('./routes/api');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//setup express app
const app = express();

//connect to mongo
const dbUrl = `mongodb://Sandy:${encodeURIComponent('aj@21071992')}@ds235169.mlab.com:35169/mydata`;
mongoose.connect(dbUrl);

app.use(bodyParser.json());
//app.set("views","./views");

app.use(express.static('images'));

//initialize routes
app.use('/api', routes);

//error handling middleware
app.use(function(err,req,res,next){
    //console.log(err);
    res.status(422).send({error:err.message});
});

//listen for request
app.listen(process.env.port || 4000, function(){
    console.log("server is running on port 4000");
})