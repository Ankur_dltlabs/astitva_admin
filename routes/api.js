const express = require('express');
const router = express.Router();
var fs = require('fs');
var pdf = require('html-pdf');
const Ninja = require('../models/ninja');
const uuidv4 = require('uuid/v4');
var rn = require('random-number');
var nodemailer = require('nodemailer');
var htmlToPdf = require('html-to-pdf');

// var twilio = require('twilio');
// var accountSid = 'AC38d5082ae0d49b95f6dd4b0768e0a922'; 
// var authToken = '211d0320202b0e89ab768ac03aaf23e3'; 
// var client = new twilio(accountSid, authToken);


//send otp
const SendOtp = require('sendotp');
const sendOtp = new SendOtp('213057AfPeN8JJ5ae6bf48');

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var Web3 = require('web3');
var Tx = require('ethereumjs-tx');
if(typeof Web3!='undefined'){
    var web3 = new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io/9Pwwtf91Nf5LxPZL7lHf'));      
}

// function base64_decode(base64str,file) {
// 	var bitmap = new Buffer(base64str,'base64');
// 	//writing into an image file
// 	fs.writeFile(file, bitmap);
// 	//write a text file
// 	 console.log('File created from base64 encoded string');
//  }

var abi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "_name",
				"type": "string"
			},
			{
				"name": "_f_name",
				"type": "string"
			},
			{
				"name": "_gender",
				"type": "string"
			},
			{
				"name": "_dob",
				"type": "string"
			},
			{
				"name": "_mob_no",
				"type": "string"
			},
			{
				"name": "_add",
				"type": "string"
			},
			{
				"name": "_email",
				"type": "string"
			},
			{
				"name": "_id",
				"type": "string"
			}
		],
		"name": "setInfo",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_id",
				"type": "string"
			},
			{
				"name": "_name",
				"type": "string"
			},
			{
				"name": "_f_name",
				"type": "string"
			},
			{
				"name": "_gender",
				"type": "string"
			},
			{
				"name": "_dob",
				"type": "string"
			},
			{
				"name": "_mob_no",
				"type": "string"
			},
			{
				"name": "_add",
				"type": "string"
			},
			{
				"name": "_email",
				"type": "string"
			}
		],
		"name": "updateInfo",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_id",
				"type": "string"
			}
		],
		"name": "getValues",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_id",
				"type": "string"
			},
			{
				"name": "_email",
				"type": "string"
			}
		],
		"name": "validateByEmail",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_id",
				"type": "string"
			},
			{
				"name": "_mob_no",
				"type": "string"
			}
		],
		"name": "validateByMob",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];

	var identityContract = web3.eth.contract(abi);
    var myContract = identityContract.at('0x52741ba5a8c67ac5a55de52efe2e7bf824906569');
    const addressFrom = '0xdcb572f3188AFC94Ca683426B5D1De11467c7202'
    var key = 'f811cdf4ff73e9d7132bd2bc09a583ba952b20ea84fb81288bc362dda3533e1c';
    var privateKey = new Buffer(key, 'hex');
	const addressTo = '0x52741ba5a8c67ac5a55de52efe2e7bf824906569'
	

	
	
//get information of a ninja from DB by Astitva ID and also for eKYC
router.get('/ninjas/:id',function(req, res){
    Ninja.find({Id_no:req.params.id},function(err,ninja){
		
        if(!err){
            //console.log(ninja[0]);
            if(ninja[0] != undefined)
			{//console.log("doing");
				console.log("Found id")
				var getInfo = myContract.getValues.call(ninja[0].Id_no);
				//console.log(getInfo);

				//var Id = ninja[0].Id_no;
				//var image = ninja[0].image;
				
				res.send({Id:ninja[0].Id_no,Image:ninja[0].image,GetInfo:getInfo});
            }else{
				console.log("Not Found id")
				res.send({Id:"error",Image:"error",GetInfo:"error"});
			}
		}else{
			console.log("ERROR",err);
			//res.send("NotExist");
			res.send({Id:"error",Image:"error",GetInfo:"error"});
		}
        
        
    });
    
});


//add a ninja into DB and blockchain
router.post('/ninjas',function(req, res, next){
    var data = req.body;
	
	//creating a unique ID
	var id_no = uuidv4();

	//saving image into a folder
	var bitmap = new Buffer(data.image,'base64');
	console.log(bitmap);
	
	fs.writeFile(`./images/`+id_no+`.png`,bitmap, 'base64', 
	function(err, data){
		if(!err){
			console.log("see image");
			
		}else{
			console.log(err)
		}
	});

	 
	var aid = `<html>
	<head>
	</head>
	<body>
		<div style="width: 600px;height: 350px;background-color: #FBBC05">

			<!-- header -->
			<div style="width: 600px;height: 60px;background-color:black">

				<div style="width: 20%;height: 60px;float:left;display:table-cell; vertical-align:middle; text-align:center">
						<img src="./images/logo.png" alt = "Astitva Logo" style="width: 50px;height: 50px;">
                </div>
                
				<div style="width: 80%;height: 60px;float:left;text-align: left">
					<h2 style="color:white;line-height: 1px">Astitva</h2>
					<p style="color:white;line-height: 1px">An Identity with blockchain</p>
                </div>
                
			</div>
	
	
			<!-- main body -->
			<div style="width: 600px;height: 240px;background-color:#FBBC05">

                <div style="width:600px;height: 192px;background-color: #FBBC05;margin-top: 8px">

                    <div style="width: 30%;height: 182px;background-color: #FBBC05;float:left;text-align: center;display:table-cell; vertical-align:middle">
                            <center><img id="image" src="./images/`+id_no+`.png" alt="person image" style="width: 120px;height: 160px;"></center>
                    </div>
                    
                    <div style="width: 70%;height: 182px;float:left;text-align: left;background-color:#FBBC05">
                            
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Name: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="name" style=" line-height: 5px;display:inline">`+data.name+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Father Name: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="fname" style=" line-height: 5px;display:inline">`+data.fname+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Gender: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="gender" style=" line-height: 5px;display:inline">`+data.gender+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">DOB: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="dob" style=" line-height: 5px;display:inline">`+data.dob+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                            <div style="width:30%;height:26px;float:left">
                                    <h4 style="line-height: 5px;display:inline">Address: </h4>
                            </div>
                            <div style="width:70%;height:26px;float:left">
                                    <p id="address" style=" line-height: 5px;display:inline">`+data.add+`</p>
                            </div>
                                
                                
                        </div>
                        
                        <div style="width:100%;height:26px;margin-top: 10px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Mobile: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="mobile" style=" line-height: 5px;display:inline"> `+data.mob+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Email: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="email" style=" line-height: 5px;display:inline">`+data.email+`</p>
                                </div>
                        </div>					
                    </div>
                </div>	

				<div style="width:600px;height: 50px;background-color: #FBBC05">					
					<center><h3>`+id_no+`</h3></center>
				</div>
					
			</div>
	
	
			<!-- footer -->
			<div style="width: 600px;height: 50px;background-color:black;display:table-cell; vertical-align:middle; text-align:center">
				<p style="color:white;line-height: 5px">This Identity is not issued by any government</p>
				<p style="color:white;line-height: 5px">Copyright &copy; DLT Labs</p>
				
			</div>
		</div>
	</body>
	</html>`;

    var doi = Date();  

    var myData = myContract.setInfo.getData(data.name,data.fname,data.gender,data.dob,data.mob,data.add,data.email,id_no);   

	var count = web3.eth.getTransactionCount(addressFrom);

    var rawTx = {
        nonce: web3.toHex(count),
        gasLimit: web3.toHex(533400),
        gasPrice: web3.toHex(10000000000,'gwei'),
        to: addressTo,
        from: addressFrom,
        //value: 0x1,
        data: myData
    }

   
    var tx = new Tx(rawTx);
    tx.sign(privateKey);
	var serializedTx = tx.serialize();

	
    web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err, hash)
    {
		//console.log("Hello1");
        if(!err){
            console.log("Transaction Hash",hash);
			waitForReceipt(hash, function(err, receipt){
					if(!err	){
						console.log("Transaction Receipt", receipt);
						if(receipt.status == "0x1"){
							Ninja.create({Id_no: id_no,Doi: doi,image: data.image,finger: data.finger},function(err,ress){
								if(!err){

									res.send({Id_no: id_no,Doi: doi});

									htmlToPdf.convertHTMLString(aid, `./id/`+id_no+`.pdf`,
										function (error, success) {
											if (error) {
													console.log('Oh noes! Errorz!');
													console.log(error);
												} else {
													console.log('Woot! Success!');
													console.log(success);
													var transporter = nodemailer.createTransport({
														service: 'gmail',
														auth: {
														user: 'knp.ankurjaiswal@gmail.com',
														pass: 'myDigiWork@21071992'
														}
														});
				
													const mailOptions = {
														from: 'knp.ankurjaiswal@gmail.com', // sender address
														to: data.email, // list of receivers
														subject: 'Your Astitva Identity', // Subject line
														//html: `<p>Hello `+data.name+`. This is your Astiva Identity.</p>`,// plain text body
														attachments:[{
														filename:"AstitvaID.pdf",
														path:`./id/`+id_no+`.pdf`,
														contentType:'application/pdf'
														}]
														};
													//transporter.sendMail(mailOptions);
													transporter.sendMail(mailOptions, function (err, info) {
															if(!err){	
																console.log("ID send successfully")
															}
															
															else{
																console.log("oops.. not send")														
															}
															
														});
												}
										});																				
									
								}
								else{
									res.send({Id_no: "DBerror",Doi: "DBerror"});
								}
							});
							
						}else{
							res.send({Id_no: "statusError",Doi: "statusError"});
						}
					}
					else{
						console.log("Receipt Error", err);
						res.send({Id_no: "receiptError",Doi: "receiptError"});
					}
			});
				
        }
        else{
			console.log("Send Transaction",err)
			//res.send("error")
			doAgain(count,1,3);
        }
    
	});
	//End of raw transaction
	
	function doAgain(currentNonce, attemptNumber, maxAttempts){
		if(currentNonce+attemptNumber === maxAttempts){
			//res.send({Id_no: "error1",Doi:"Failed after" + maxAttempts + " attempts. Try after some time"});
			res.send({Id_no: "nonceError",Doi: "nonceError1"});
		}
		var rawTx = {
			nonce: web3.toHex(currentNonce+attemptNumber),
			gasLimit: web3.toHex(533400),
			gasPrice: web3.toHex(10000000000,'gwei'),
			to: addressTo,
			from: addressFrom,
			//value: 0x1,
			data: myData
		}
	
	   
		var tx = new Tx(rawTx);
		tx.sign(privateKey);
		var serializedTx = tx.serialize();
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err, hash)
		{
			if(!err){
				console.log("Transaction Hash",hash);
				waitForReceipt(hash, function(err, receipt){
					if(!err){
						console.log("Transaction Receipt", receipt);
						if(receipt.status == "0x1"){
							
						
							Ninja.create({Id_no: id_no,Doi: doi,image: data.image,finger: data.finger},function(err,ress){
								if(!err){
									res.send({Id_no: id_no,Doi: doi});

									htmlToPdf.convertHTMLString(aid, `./id/`+id_no+`.pdf`,
										function (error, success) {
											if (error) {
													console.log('Oh noes! Errorz!');
													console.log(error);
												} else {
													console.log('Woot! Success!');
													console.log(success);
													var transporter = nodemailer.createTransport({
														service: 'gmail',
														auth: {
														user: 'knp.ankurjaiswal@gmail.com',
														pass: 'myDigiWork@21071992'
														}
														});
				
													const mailOptions = {
														from: 'knp.ankurjaiswal@gmail.com', // sender address
														to: data.email, // list of receivers
														subject: 'Your Astitva Identity', // Subject line
														//html: `<p>Hello `+data.name+`. This is your Astiva Identity.</p>`,// plain text body
														attachments:[{
														filename:"AstitvaID.pdf",
														path:`./id/`+id_no+`.pdf`,
														contentType:'application/pdf'
														}]
														};
													//transporter.sendMail(mailOptions);
													transporter.sendMail(mailOptions, function (err, info) {
															if(!err){	
																console.log("ID send successfully")
															}
															
															else{
																console.log("oops.. not send")														
															}
															
														});
												}
										});
								}
								else{
									res.send({Id_no: "DBerror",Doi: "DBerror"});
								}
							});
							
						}else{
							res.send({Id_no: "statusError",Doi: "statusError"});
						}
					}
					else{
						console.log("Receipt Error", err);
						res.send({Id_no: "receiptError",Doi: "receiptError"});
					}
			});  
			}
			else{
				//console.log("Send Transaction",err)
				//res.send("error")
				doAgain(currentNonce, attemptNumber+1, 3);
			}
		
		});
	}
	//End of doAgain



	function waitForReceipt(hash, cb) {
		web3.eth.getTransactionReceipt(hash, function(err, receipt) {
			console.log("Waiting for receipt...............");
		   if (err) {
			 console.log("Something wrong");
		   }
	  
		   if (receipt !== null) {
			// Transaction went through
			if (cb) {
			 // console.log("receipt: ", receipt)
			 cb(false,receipt);
			}
		   } else {
			// Try again in 1 second
			setTimeout(function() {
			 waitForReceipt(hash, cb);
			}, 1000);
		   }
		});
	}

});

//update ninja into  only on blockchain
router.put('/ninjas/:id',function(req, res){

	Ninja.find({Id_no:req.params.id},function(err,ninja){
		var id_no = ninja[0].Id_no;
		var person = req.body;
		console.log(person)

		var aid = `<html>
	<head>
	</head>
	<body>
		<div style="width: 600px;height: 350px;background-color: #FBBC05">

			<!-- header -->
			<div style="width: 600px;height: 60px;background-color:black">

				<div style="width: 20%;height: 60px;float:left;display:table-cell; vertical-align:middle; text-align:center">
						<img src="./images/logo.png" alt = "Astitva Logo" style="width: 50px;height: 50px;">
                </div>
                
				<div style="width: 80%;height: 60px;float:left;text-align: left">
					<h2 style="color:white;line-height: 1px">Astitva</h2>
					<p style="color:white;line-height: 1px">An Identity with blockchain</p>
                </div>
                
			</div>
	
	
			<!-- main body -->
			<div style="width: 600px;height: 240px;background-color:#FBBC05">

                <div style="width:600px;height: 192px;background-color: #FBBC05;margin-top: 8px">

                    <div style="width: 30%;height: 182px;background-color: #FBBC05;float:left;text-align: center;display:table-cell; vertical-align:middle">
                            <center><img id="image" src="./images/`+id_no+`.png" alt="person image" style="width: 120px;height: 160px;"></center>
                    </div>
                    
                    <div style="width: 70%;height: 182px;float:left;text-align: left;background-color:#FBBC05">
                            
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Name: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="name" style=" line-height: 5px;display:inline">`+person.name+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Father Name: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="fname" style=" line-height: 5px;display:inline">`+person.fname+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Gender: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="gender" style=" line-height: 5px;display:inline">`+person.gender+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">DOB: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="dob" style=" line-height: 5px;display:inline">`+person.dob+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                            <div style="width:30%;height:26px;float:left">
                                    <h4 style="line-height: 5px;display:inline">Address: </h4>
                            </div>
                            <div style="width:70%;height:26px;float:left">
                                    <p id="address" style=" line-height: 5px;display:inline">`+person.add+`</p>
                            </div>
                                
                                
                        </div>
                        
                        <div style="width:100%;height:26px;margin-top: 10px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Mobile: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="mobile" style=" line-height: 5px;display:inline"> `+person.mob+`</p>
                                </div>
                        </div>
                        
                        <div style="width:100%;height:26px">
                                <div style="width:30%;height:26px;float:left">
                                        <h4 style="line-height: 5px;display:inline">Email: </h4>
                                </div>
                                <div style="width:70%;height:26px;float:left">
                                        <p id="email" style=" line-height: 5px;display:inline">`+person.email+`</p>
                                </div>
                        </div>					
                    </div>
                </div>	

				<div style="width:600px;height: 50px;background-color: #FBBC05">					
					<center><h3>`+id_no+`</h3></center>
				</div>
					
			</div>
	
	
			<!-- footer -->
			<div style="width: 600px;height: 50px;background-color:black;display:table-cell; vertical-align:middle; text-align:center">
				<p style="color:white;line-height: 5px">This Identity is not issued by any government</p>
				<p style="color:white;line-height: 5px">Copyright &copy; DLT Labs</p>
				
			</div>
		</div>
	</body>
	</html>`;

		//console.log(person.name);

		var myData = myContract.updateInfo.getData(id_no,person.name,person.fname,person.gender,person.dob,person.mob,person.add,person.email);   
		//console.log(count)

		var count = web3.eth.getTransactionCount(addressFrom);

    var rawTx = {
        nonce: web3.toHex(count),
        gasLimit: web3.toHex(533400),
        gasPrice: web3.toHex(10000000000,'gwei'),
        to: addressTo,
        from: addressFrom,
        //value: 0x1,
        data: myData
    }

   
    var tx = new Tx(rawTx);
    tx.sign(privateKey);
    var serializedTx = tx.serialize();
    web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err, hash)
    {
        if(!err){
            console.log("Transaction Hash",hash);
               				
				waitForReceipt(hash, function(err, receipt){
					if(!err){
						console.log("Transaction Receipt", receipt);
						if(receipt.status == "0x1"){
							res.send({Status:"Done"});
							//create updated id and send to email id
							htmlToPdf.convertHTMLString(aid, `./id/`+id_no+`.pdf`,
										function (error, success) {
											if (error) {
													console.log('Oh noes! Errorz!');
													console.log(error);
												} else {
													console.log('Woot! Success!');
													console.log(success);
													var transporter = nodemailer.createTransport({
														service: 'gmail',
														auth: {
														user: 'knp.ankurjaiswal@gmail.com',
														pass: 'myDigiWork@21071992'
														}
														});
				
													const mailOptions = {
														from: 'knp.ankurjaiswal@gmail.com', // sender address
														to: person.email, // list of receivers
														subject: 'Your Updated Astitva Identity', // Subject line
														//html: `<p>Hello `+data.name+`. This is your Astiva Identity.</p>`,// plain text body
														attachments:[{
														filename:"AstitvaID.pdf",
														path:`./id/`+id_no+`.pdf`,
														contentType:'application/pdf'
														}]
														};
													//transporter.sendMail(mailOptions);
													transporter.sendMail(mailOptions, function (err, info) {
															if(!err){	
																console.log("ID send successfully")
															}
															
															else{
																console.log("oops.. not send")														
															}
															
														});
												}
										});	

						
						}else{
							res.send({Status:"error"});
						}
					}
					else{
						console.log("Receipt Error", err);
						res.send({Status:"error"});
					}
			    });
        }
        else{
			//console.log("Send Transaction",err)
			//res.send("NonceLow")
			doAgain(count,1,3);
        }
    
	});
	
	function doAgain(currentNonce, attemptNumber, maxAttempts){
		if(currentNonce+attemptNumber === maxAttempts){
			//res.send({Id_no: "error1",Doi:"Failed after" + maxAttempts + " attempts. Try after some time"});
			//res.send({Id_no: "nonceError",Doi: "nonceError1"});
			res.send({Status:"nonceError"});
		}
		var rawTx = {
			nonce: web3.toHex(currentNonce+attemptNumber),
			gasLimit: web3.toHex(533400),
			gasPrice: web3.toHex(10000000000,'gwei'),
			to: addressTo,
			from: addressFrom,
			//value: 0x1,
			data: myData
		}
	
	   
		var tx = new Tx(rawTx);
		tx.sign(privateKey);
		var serializedTx = tx.serialize();
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err, hash)
		{
			if(!err){
				console.log("Transaction Hash",hash);
					
					waitForReceipt(hash, function(err, receipt){
						if(!err){
							console.log("Transaction Receipt", receipt);
							if(receipt.status == "0x1"){
								res.send({Status:"Done"});
								
								
							}else{
								res.send({Status:"error"});
							}
						}
						else{
							console.log("Receipt Error", err);
							res.send({Status:"error"});
						}
					});
			}
			else{
				console.log("Send Transaction",err)
				doAgain(currentNonce, attemptNumber+1, 3);
			}
		
		});
	}

	function waitForReceipt(hash, cb) {
		web3.eth.getTransactionReceipt(hash, function(err, receipt) {
			console.log("Waiting for receipt...............");
		   if (err) {
			 console.log("Something wrong");
		   }
	  
		   if (receipt !== null) {
			// Transaction went through
			if (cb) {
			 // console.log("receipt: ", receipt)
			 cb(false,receipt);
			}
		   } else {
			// Try again in 1 second
			setTimeout(function() {
			 waitForReceipt(hash, cb);
			}, 1000);
		   }
		});
	}

	});
    
});

//---------------------All verification part will done here---------------------------------
//check existance of ID
router.get('/verify/:id',function(req, res){
    Ninja.find({Id_no:req.params.id},function(err,ninja){
		
        if(!err){
            
            if(ninja[0] != undefined)
			{
				//console.log("Found something")
				var getInfo = myContract.getValues.call(ninja[0].Id_no);
				//console.log(ninja[0].Id_no,getInfo[4],getInfo[6],ninja[0].finger);
				
				res.send({Id:ninja[0].Id_no,Mob:getInfo[4],Email:getInfo[6],Finger:ninja[0].finger});
            }else{
				console.log("Not Found id")
				res.send({Id:"NotFound",Mob:"NotFound",Email:"NotFound",Finger:"NotFound"});
			}
		}else{
			console.log("ERROR",err);
			res.send({Id:"error",Mob:"error",Email:"error",Finger:"error"});
		}
        
        
    });
    
});


// sending OTP to email
router.get('/email/:Email/:Id_no',function(req,res){
	//console.log(req.params.Email);
	//console.log("doing");
	//console.log(req.params.Id_no);
	var gen = rn.generator({
		min:  1000,
	    max:  9999,
	    integer: true
	  })
	  var ran = gen()
	  //console.log(ran);

	  var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			   user: 'knp.ankurjaiswal@gmail.com',
			   pass: 'myDigiWork@21071992'
		   }
	   });

	   const mailOptions = {
		from: 'knp.ankurjaiswal@gmail.com', // sender address
		to: req.params.Email, // list of receivers
		subject: 'Verification of Astitva Identity', // Subject line
		html: `<p>Your one time password is:`+ran+`</p>`// plain text body
	  };

	  transporter.sendMail(mailOptions, function (err, info) {
		if(!err){		  
		  var myQuery = {Id_no:req.params.Id_no};
		  var Otp = {$set: { otp: ran}};
		  Ninja.updateOne(myQuery, Otp,function(err,ress){
			  if(!err){
				res.send({Status:"success"});
			  }
			  else{
				  res.send({Status:"error"});
			  }
		  })
		}
		  
		else{
			console.log(info);
		  res.send({Status:"WrongEmail"});
		}
		  
	 });
})

//semding otp to mobile

router.get('/mobile/:Mob/:Id_no',function(req,res){

	var gen = rn.generator({
		min:  1000,
	    max:  9999,
	    integer: true
	  })
	  var ran = gen()

	  sendOtp.send('+91'+req.params.Mob, "ASTITVA",ran, function (error, data, response) {
		console.log(data);
		if(!error){
			var myQuery = {Id_no:req.params.Id_no};
			   var Otp = {$set: { otp: ran}};
			   Ninja.updateOne(myQuery, Otp,function(err,ress){
					if(!err){
					res.send({Status:"success"});
				    }
		     	    else{
						res.send({Status:"error"});
					}
				})
		}
	  });

	//   client.messages.create({
	// 	body: "OTP for Astitva ID: "+ ran,
	// 	to: '+91'+req.params.Mob,  // Text this number
	// 	from: '+17206507461' // From a valid Twilio number
	// },function(err,message){
	// 	if(!err){
	// 		console.log(message.sid);
	// 		var myQuery = {Id_no:req.params.Id_no};
	// 	  	var Otp = {$set: { otp: ran}};
	// 	 	Ninja.updateOne(myQuery, Otp,function(err,ress){
	// 		  if(!err){
	// 			res.send({Status:"success"});
	// 		  }
	// 		  else{
	// 			  res.send({Status:"error"});
	// 		  }
	// 	  })
	// 	}else{
	// 		//console.log(message.sid);
	// 	  	res.send({Status:"WrongMobile"});
	// 	}
	// })
	//.then((message) => console.log(message.sid));



})

// Verify OTP

router.get('/emailOtp/:Id_no/:Otp',function(req,res){
	var id = req.params.Id_no;
	var otp_received = req.params.Otp;
	//console.log(id)
	//console.log(otp_received)
	Ninja.find({Id_no:id},function(err,ninja){
		if(!err){
			//console.log(ninja);
			//console.log(ninja[0].otp);
			if(ninja[0].otp == otp_received){
				var getInfo = myContract.getValues.call(id);
				res.send({Id:id,Image:ninja[0].image,GetInfo:getInfo});
			}
			else{
				res.send({Id:"NotMatch",Image:"NotMatch",GetInfo:"NotMatch"});
			}
		}
		else{
			res.send({Id:"error",Image:"error",GetInfo:"error"});

		}
	})
})



// expose this routes
module.exports = router;
