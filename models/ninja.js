const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create ninja schema & model

const NinjaSchema = new Schema({
    Id_no: String,
    Doi: String,
    image: String,
    finger: String,
    otp: Number
});

const Ninja = mongoose.model('ninja',NinjaSchema);

module.exports = Ninja;